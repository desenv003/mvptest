package com.example.demo.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import static org.mockito.Mockito.when;

public abstract class AbstractIntegrationTest {
    @Autowired
    @MockBean
    public Clock clock;

    public void setDate(int year, Month month, int dayOfMonth, int hour, int minute, int second) {
        LocalDateTime localDateTime = getDateTest(year, month, dayOfMonth, hour, minute, second);
        Clock fixed = Clock.fixed(localDateTime.toInstant(ZoneOffset.of("-03:00")), ZoneId.systemDefault());

        when(clock.instant()).thenReturn(fixed.instant());
        when(clock.getZone()).thenReturn(fixed.getZone());
    }

    private LocalDateTime getDateTest(int year, Month month, int dayOfMonth, int hour, int minute, int second) {
        return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
    }
}