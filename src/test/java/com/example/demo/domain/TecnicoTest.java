package com.example.demo.domain;

import com.example.demo._environments.EnvTecnico;
import com.example.demo.config.FunctionalTest;
import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.model.Tecnico;
import com.example.demo.domain.repositories.TecnicoRepository;
import com.example.demo.domain.services.TecnicoService;
import com.example.demo.domain.utils.BooleanTypeUtils;
import com.example.demo.domain.utils.StringTypeUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@FunctionalTest
public class TecnicoTest {
    private static final String ESTE_NOME_DEVERIA_SER_VALIDO = "Este nome é válido!";
    private static final String ESTE_NOME_DEVERIA_SER_INVALIDO = "Este nome é inválido!";
    private static final String ESTA_DISPONIBILIDADE_DEVERIA_SER_VALIDA = "Esta disponibilidade é válido!";

    @Autowired
    private TecnicoService service;

    @Autowired
    private EnvTecnico envTecnico;

    @Autowired
    private TecnicoRepository repository;

    @BeforeEach
    void setup() throws RecordFoundException {
        envTecnico.init();
    }

    @Test
    void happyDay() {
        Assert.assertEquals("Número de técnicos: ", 1, service.find().size());
    }

    @Test
    void shouldBeUpdateDisponibilidade() throws RecordFoundException {
        Tecnico tecnico = service.find("user1");
        service.update(tecnico, true, "user0");

        assertEquals("user0", tecnico.getName());
        assertTrue(tecnico.getDisponibilidade());
    }

    @Test
    void shouldBeInvalidateSpecialCharacters() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("&!#&(%¨#_-$!?]"));
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("R*nald*"));
    }

    @Test
    void shouldBeInvalidateNumberCharacters() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("R0nald0"));
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("69018982934"));
    }

    @Test
    void shouldBeInvalidateNull() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validate(null));
    }

    @Test
    void shouldBeInvalidateEmpty() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validate(""));
    }

    @Test
    void shouldBeValidate() {
        assertTrue(ESTE_NOME_DEVERIA_SER_VALIDO, StringTypeUtils.validate("João"));
    }

    @Test
    void shouldBeValidateDisponibilidadeFalse() {
        assertFalse(ESTA_DISPONIBILIDADE_DEVERIA_SER_VALIDA, BooleanTypeUtils.validate(false));
    }

    @Test
    void shouldBeValidateDisponibilidadeTrue() {
        assertTrue(ESTA_DISPONIBILIDADE_DEVERIA_SER_VALIDA, BooleanTypeUtils.validate(true));
    }

    @Test
    void shouldFoundTecnico() throws RecordFoundException {
        Tecnico tecnico = repository.findByName("user1");
        assertNotNull(tecnico);
    }

    @Test
    void shouldDeleteOneItem() {
        Tecnico tecnico = service.find("user1");
        service.delete(tecnico);
        assertEquals(0, service.find().size());
    }
}