package com.example.demo.domain.utils;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.Date;
import static org.junit.Assert.assertEquals;

class DateUtilsTest {
    @Test
    void getParse() {
        LocalDate dataLocal = DateUtils.getParse("01/12/2016");

        assertEquals("Ano", 2016, dataLocal.getYear());
        assertEquals("Mes", 12, dataLocal.getMonthValue());
        assertEquals("Dia", 1, dataLocal.getDayOfMonth());
    }

    @Test
    void getDate() {
        LocalDate now = DateUtils.getParse("01/12/2016");
        Date date = DateUtils.getDate(now);

        assertEquals("toDate", "Thu Dec 01 00:00:00 BRST 2016", date.toString());
    }

    @Test
    void getDateString() {
        Date date = DateUtils.getDate("01/12/2016");

        assertEquals("toDate", "Thu Dec 01 00:00:00 BRST 2016", date.toString());
    }

    @Test
    void getLocalDate() {
        Date date = DateUtils.getDate("01/12/2016");

        assertEquals("toDate", "2016-12-01", DateUtils.getLocalDate(date).toString());
    }

    @Test
    void getParseTime() {
        String date = "01/12/2016 10:10";

        assertEquals("Ano", "2016-12-01T10:10", DateUtils.getParseTime(date).toString());
    }

    @Test
    void getLocalDateTime() {
        Date date = DateUtils.getDate("01/12/2016");

        assertEquals("toDate", "2016-12-01T00:00", DateUtils.getLocalDateTime(date).toString());
    }
}