package com.example.demo.domain.utils;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

class StringUtilsTest {
    @Test
    void deveRemoverAcentos() {
        String entrada = "Ação necessária para certificação";
        String saidaEsperada = "Acao necessaria para certificacao";

        assertEquals(saidaEsperada, StringUtils.removerAcentos(entrada));
    }

    @Test
    void deveRetornarMesmaEntrada() {
        String entrada = "Acao necessaria para certificacao";

        assertEquals(entrada, StringUtils.removerAcentos(entrada));
    }
}