package com.example.demo.domain;

import com.example.demo._environments.EnvServico;
import com.example.demo.config.FunctionalTest;
import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.model.Cliente;
import com.example.demo.domain.model.Servico;
import com.example.demo.domain.model.Tecnico;
import com.example.demo.domain.repositories.ClienteRepository;
import com.example.demo.domain.repositories.ServicoRepository;
import com.example.demo.domain.repositories.TecnicoRepository;
import com.example.demo.domain.services.ServicoService;
import com.example.demo.domain.utils.DateTypeUtils;
import com.example.demo.domain.utils.DateUtils;
import com.example.demo.domain.utils.StringTypeUtils;
import com.example.demo.domain.utils.BooleanTypeUtils;
import com.example.demo.domain.utils.IntegerTypeUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintViolationException;
import java.util.Date;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@FunctionalTest
public class ServicoTest {
    private static final String ESTE_STATUS_DEVERIA_SER_VALIDO = "Este status é válido!";
    private static final String ESTE_STATUS_DEVERIA_SER_INVALIDO = "Este status é inválido!";
    private static final String ESTA_PROCEDENCIA_DEVERIA_SER_VALIDA = "Esta procedência é válida!";
    private static final String ESTE_IDCLIENTE_DEVERIA_SER_INVALIDO = "Este ID do cliente é inválido!";
    private static final String ESTE_IDCLIENTE_DEVERIA_SER_VALIDO = "Este ID do cliente é válido!";
    private static final String ESTA_DATATECNICO_DEVERIA_SER_VALIDA = "Esta data do ténico é válida!";
    private static final String ESTA_DATATECNICO_DEVERIA_SER_INVALIDA = "Esta data do ténico é inválida!";

    @Autowired
    private ServicoService service;

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private TecnicoRepository tecnicoRepository;

    @Autowired
    private EnvServico envServico;

    @Autowired
    private ServicoRepository repository;

    @BeforeEach
    void setup() throws RecordFoundException {
        envServico.init();
    }

    @Test
    void happyDay() {
        Assert.assertEquals("Número de serviços: ", 1, service.find().size());
    }

    @Test
    void shouldBeUpdateServico() throws RecordFoundException {
        Servico servico = service.find(1);
        repository.findByCliente(clienteRepository.findByName("user1"));
        repository.findByTecnico(tecnicoRepository.findByName("user1"));

        Date dateServico = DateUtils.getDate("01/12/2016");
        Date dateTecnico = DateUtils.getDate("01/12/2016");

        Cliente cliente = clienteRepository.findByName("user1");
        Tecnico tecnico = tecnicoRepository.findByName("user1");

        service.update(servico,
                0,
                "Finalizada",
                true,
                cliente,
                tecnico,
                DateUtils.getLocalDate(dateServico),
                DateUtils.getLocalDate(dateTecnico));

        assertEquals(0, servico.getNumeroServico());
        assertEquals("Finalizada", servico.getStatus());
        assertTrue(servico.getProcedencia());
        assertEquals(cliente, servico.getCliente());
        assertEquals(tecnico, servico.getTecnico());
        assertEquals(DateUtils.getLocalDate(dateServico), servico.getDataSolicitacao());
        assertEquals(DateUtils.getLocalDate(dateTecnico), servico.getDataTecnico());
    }

    @Test
    void shouldBeInvalidateSpecialCharacters() {
        assertFalse(ESTE_STATUS_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("&!#&(%¨#_-$!?]"));
        assertFalse(ESTE_STATUS_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("@b&rto"));
    }

    @Test
    void shouldBeInvalidateNumberCharacters() {
        assertFalse(ESTE_STATUS_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("Abert0*"));
        assertFalse(ESTE_STATUS_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("69018982934"));
    }

    @Test
    void shouldBeInvalidateNull() {
        assertFalse(ESTE_STATUS_DEVERIA_SER_INVALIDO, StringTypeUtils.validate(null));
    }

    @Test
    void shouldBeInvalidateEmpty() {
        assertFalse(ESTE_STATUS_DEVERIA_SER_INVALIDO, StringTypeUtils.validate(""));
    }

    @Test
    void shouldBeValidate() {
        assertTrue(ESTE_STATUS_DEVERIA_SER_VALIDO, StringTypeUtils.validate("Aberto"));
    }

    @Test
    void shouldBeValidateProcedenciaFalse() {
        assertFalse(ESTA_PROCEDENCIA_DEVERIA_SER_VALIDA, BooleanTypeUtils.validate(false));
    }

    @Test
    void shouldBeValidateProcedenciaTrue() {
        assertTrue(ESTA_PROCEDENCIA_DEVERIA_SER_VALIDA, BooleanTypeUtils.validate(true));
    }

    @Test
    void shouldBeInvalidateClienteID() {
        assertFalse(ESTE_IDCLIENTE_DEVERIA_SER_INVALIDO, IntegerTypeUtils.validate(-1));
    }

    @Test
    void shouldBeValidateClienteID() {
        assertTrue(ESTE_IDCLIENTE_DEVERIA_SER_VALIDO, IntegerTypeUtils.validate(1));
    }

    @Test
    void shouldBeValidateDataTecnico() {
        assertTrue(ESTA_DATATECNICO_DEVERIA_SER_VALIDA, DateTypeUtils.validate("01/12/2016"));
    }

    @Test
    void shouldBeInvalidateDataTecnicoByFormat() {
        assertFalse(ESTA_DATATECNICO_DEVERIA_SER_INVALIDA, DateTypeUtils.validate("01-12-2016"));
        assertFalse(ESTA_DATATECNICO_DEVERIA_SER_INVALIDA, DateTypeUtils.validate("01.12.2016"));
    }

    @Test
    void shouldFoundServicoByNumber() throws RecordFoundException {
        Servico servico = repository.findByNumeroServico(1);

        assertNotNull(servico);
    }

    @Test
    void shouldDeleteOneItem() {
        Servico servico = service.find(1);
        service.delete(servico);

        assertEquals(0, service.find().size());
    }

    @Test
    void shouldNotSaveServicoWithoutClienteAndTecnico() {
        Date dateServico = DateUtils.getDate("01/12/2016");
        Date dateTecnico = DateUtils.getDate("01/12/2016");

        assertThrows(ConstraintViolationException.class,
                () -> service.save(
                        1,
                        "Aberto",
                        true,
                        (Cliente) null,
                        (Tecnico) null,
                        DateUtils.getLocalDate(dateServico),
                        DateUtils.getLocalDate(dateTecnico)
                ),"Cliente and Tecnico can't be null"
        );
    }
}