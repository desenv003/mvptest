package com.example.demo.domain;

import com.example.demo._environments.EnvItem;
import com.example.demo.config.FunctionalTest;
import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.model.Item;
import com.example.demo.domain.model.Servico;
import com.example.demo.domain.repositories.ItemRepository;
import com.example.demo.domain.repositories.ServicoRepository;
import com.example.demo.domain.services.ItemService;
import com.example.demo.domain.utils.DateUtils;
import com.example.demo.domain.utils.IntegerTypeUtils;
import com.example.demo.domain.utils.StringTypeUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@FunctionalTest
public class ItemTest {
    private static final String ESTA_DESCRICAO_DEVERIA_SER_VALIDA = "Esta descrição é válida!";
    private static final String ESTA_DESCRICAO_DEVERIA_SER_INVALIDA = "Esta descrição é inválida!";
    private static final String ESTE_IDSERVICO_DEVERIA_SER_INVALIDO = "Este ID do serviço é inválido!";
    private static final String ESTE_IDSERVICO_DEVERIA_SER_VALIDO = "Este ID do serviço é válido!";

    @Autowired
    private ItemService service;

    @Autowired
    private ServicoRepository servicoRepository;

    @Autowired
    private EnvItem envItem;

    @Autowired
    private ItemRepository repository;

    @BeforeEach
    void setup() throws RecordFoundException {
        envItem.init();
    }

    @Test
    void happyDay() {
        Assert.assertEquals("Número(s) de item(ns): ", 1, service.find().size());
    }

    @Test
    void shouldBeUpdateDescAndServico() throws RecordFoundException {
        Servico servico = servicoRepository.findByNumeroServico(1);
        Item item = service.find(servico);
        service.update(item, "Piso quebrado", servico);

        assertEquals("Piso quebrado", item.getDesc());
        assertEquals(servico, item.getServico());
    }

    @Test
    void shouldBeInvalidateSpecialCharacters() {
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validateSpecial("&!#&(%¨#_-$!?]"));
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validateSpecial("Piso r@chado"));
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validateSpecial("✌"));
    }

    @Test
    void shouldBeInvalidateNumberCharacters() {
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validateSpecial("Pis0 rachad0*"));
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validateSpecial("69018982934"));
    }

    @Test
    void shouldBeInvalidateServicoID() {
        assertFalse(ESTE_IDSERVICO_DEVERIA_SER_INVALIDO, IntegerTypeUtils.validate(-1));
    }

    @Test
    void shouldBeValidateServicoID() {
        assertTrue(ESTE_IDSERVICO_DEVERIA_SER_VALIDO, IntegerTypeUtils.validate(1));
    }

    @Test
    void shouldBeInvalidateNull() {
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validate(null));
    }

    @Test
    void shouldBeInvalidateEmpty() {
        assertFalse(ESTA_DESCRICAO_DEVERIA_SER_INVALIDA, StringTypeUtils.validate(""));
    }

    @Test
    void shouldBeValidate() {
        assertTrue(ESTA_DESCRICAO_DEVERIA_SER_VALIDA, StringTypeUtils.validate("Parede rechando"));
    }

    @Test
    void shouldFoundItemByServico() throws RecordFoundException {
        Item item = repository.findByServico(servicoRepository.findByNumeroServico(1));

        assertNotNull(item);
    }

    @Test
    void shouldDeleteOneItem() {
        Item item = service.find(servicoRepository.findByNumeroServico(1));
        service.delete(item);

        assertEquals(0, service.find().size());
    }

    @Test
    void shouldNotSaveServicoWithoutServico() {
        DateUtils.getDate("01/12/2016");
        DateUtils.getDate("01/12/2016");

        assertThrows(ConstraintViolationException.class,
                () -> service.save(
                        "Parede rachando",
                        (Servico) null
                ),"Cliente and Tecnico can't be null"
        );
    }
}