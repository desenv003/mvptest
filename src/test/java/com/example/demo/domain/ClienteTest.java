package com.example.demo.domain;

import com.example.demo._environments.EnvCliente;
import com.example.demo.config.FunctionalTest;
import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.model.Cliente;
import com.example.demo.domain.repositories.ClienteRepository;
import com.example.demo.domain.services.ClienteService;
import com.example.demo.domain.utils.StringTypeUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@FunctionalTest
class ClienteTest {
    private static final String ESTE_NOME_DEVERIA_SER_VALIDO = "Este nome é válido!";
    private static final String ESTE_NOME_DEVERIA_SER_INVALIDO = "Este nome é inválido!";

    @Autowired
    private ClienteService service;

    @Autowired
    private EnvCliente envCliente;

    @Autowired
    private ClienteRepository repository;

    @BeforeEach
    void setup() throws RecordFoundException {
        envCliente.init();
    }

    @Test
    void happyDay() throws RecordFoundException {
        assertInitial();
    }

    @Test
    void shouldBeUpdateName() throws RecordFoundException {
        Cliente cliente = service.find("user1");
        service.update(cliente, "user0");

        assertEquals("user0", cliente.getName());
    }

    @Test
    void shouldBeInvalidateSpecialCharacters() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("&!#&(%¨#_-$!?]"));
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("R*nald*"));
    }

    @Test
    void shouldBeInvalidateNumberCharacters() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("R0nald0"));
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validateSpecial("69018982934"));
    }

    @Test
    void shouldBeInvalidateNull() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validate(null));
    }

    @Test
    void shouldBeInvalidateEmpty() {
        assertFalse(ESTE_NOME_DEVERIA_SER_INVALIDO, StringTypeUtils.validate(""));
    }

    @Test
    void shouldBeValidate() {
        assertTrue(ESTE_NOME_DEVERIA_SER_VALIDO, StringTypeUtils.validate("Ana"));
    }

    @Test
    void shouldFoundCliente() throws RecordFoundException {
        Cliente cliente = repository.findByName("user1");

        assertNotNull(cliente);
    }

    @Test
    void shouldDeleteOneItem() {
        Cliente cliente = service.find("user1");
        service.delete(cliente);

        assertEquals(1, service.find().size());
    }

    @Test
    void shouldNotCreateClienteExisting() {
        assertThrows(RecordFoundException.class,
                () -> service.save("user1"),
                "This record exists in database"
        );
    }

    /*@Test
    void shouldInsertNameJoseDaSilva() throws RecordFoundException {
        service.save("José da Silva");
        Cliente jose = service.find("Jose");
        assertEquals("JOSE DA SILVA", jose.getName());
    }

    @Test
    void shouldInsertNameJoseDaSilvaLowerCase() throws RecordFoundException {
        service.save("José da Silva");
        Cliente jose = service.find("josé");
        assertEquals("JOSE DA SILVA", jose.getName());
    }*/

    private Cliente setCliente(String name) throws RecordFoundException {
        return service.save(name);
    }

    private void assertInitial() throws RecordFoundException {
        // Cria dois usuários:
        setCliente("user3");
        setCliente("user4");
        // Já existe 2 usuários, com mais dois criados, ficam 4:
        assertEquals(4, service.find().size());
    }
}