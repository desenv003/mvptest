package com.example.demo;

import java.time.LocalDateTime;
import java.time.Month;
import com.example.demo.config.FunctionalTest;
import com.example.demo.core.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

@FunctionalTest
class ClockTest extends AbstractIntegrationTest {
    @Test
    void shouldBe20190520AtNineClock() {
        setDate(2019, Month.of(5), 20, 9, 0, 0);
        assertEquals("2019-05-20T09:00", LocalDateTime.now(clock).toString());
    }

    @Test
    void shouldBe20190520AtTenClock() {
        setDate(2019, Month.of(5), 20, 10, 0, 0);
        assertEquals("2019-05-20T10:00", LocalDateTime.now(clock).toString());
    }

    @Test
    void shouldBe20200424AtTenAndFifteenClock() {
        setDate(2020, Month.of(4), 24, 10, 15, 20);
        assertEquals("2020-04-24T10:15:20", LocalDateTime.now(clock).toString());
    }
}