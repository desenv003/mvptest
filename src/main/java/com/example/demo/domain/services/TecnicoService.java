package com.example.demo.domain.services;

import java.util.List;
import com.example.demo.domain.model.Tecnico;
import com.example.demo.domain.repositories.TecnicoRepository;
import com.example.demo.domain.utils.BooleanTypeUtils;
import com.example.demo.domain.utils.StringTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TecnicoService {
    @Autowired
    private TecnicoRepository repository;

    public Tecnico save(String name, boolean disponibilidade) {
        Tecnico tecnico = Tecnico.builder()
                .name(name)
                .disponibilidade(disponibilidade)
                .build();

        return repository.saveAndFlush(tecnico);
    }

    @Transactional
    public void update(Tecnico tecnico, boolean newDisponibilidade, String newName) {
        if(BooleanTypeUtils.validate(newDisponibilidade) && StringTypeUtils.validate(newName)) {
            tecnico.setName(newName);
            tecnico.setDisponibilidade(newDisponibilidade);
            repository.save(tecnico);
        }
    }

    public void delete(Tecnico tecnico) {
        repository.delete(tecnico);
    }

    public List<Tecnico> find() {
        return repository.findAll();
    }

    public Tecnico find(String name) {
        return repository.findByName(name);
    }
}