package com.example.demo.domain.services;

import java.time.LocalDate;
import java.util.List;
import com.example.demo.domain.model.Cliente;
import com.example.demo.domain.model.Servico;
import com.example.demo.domain.model.Tecnico;
import com.example.demo.domain.repositories.ServicoRepository;
import com.example.demo.domain.utils.BooleanTypeUtils;
import com.example.demo.domain.utils.IntegerTypeUtils;
import com.example.demo.domain.utils.StringTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServicoService {
    @Autowired
    private ServicoRepository repository;

    public Servico save(int numeroServico, String status, boolean procedencia, Cliente cliente, Tecnico tecnico, LocalDate dataSolicitacao, LocalDate dataTecnico) {
        Servico servico = Servico.builder()
                .numeroServico(numeroServico)
                .status(status)
                .procedencia(procedencia)
                .cliente(cliente)
                .tecnico(tecnico)
                .dataSolicitacao(dataSolicitacao)
                .dataTecnico(dataTecnico)
                .build();

        return repository.saveAndFlush(servico);
    }

    @Transactional
    public void update(Servico servico, Integer newNum, String newStatus, boolean newProcedencia, Cliente newCliente, Tecnico newTecnico, LocalDate newDataSolicitacao, LocalDate newDataTecnico) {
        if(IntegerTypeUtils.validate(newNum) && StringTypeUtils.validate(newStatus) && BooleanTypeUtils.validate(newProcedencia)) {
            servico.setNumeroServico(newNum);
            servico.setStatus(newStatus);
            servico.setProcedencia(newProcedencia);
            servico.setCliente(newCliente);
            servico.setTecnico(newTecnico);
            servico.setDataSolicitacao(newDataSolicitacao);
            servico.setDataTecnico(newDataTecnico);
            repository.save(servico);
        }
    }

    public void delete(Servico servico) {
        repository.delete(servico);
    }

    public List<Servico> find() {
        return repository.findAll();
    }

    public Servico find(int numeroServico) {
        return repository.findByNumeroServico(numeroServico);
    }
    public Servico find(Cliente cliente) {
        return repository.findByCliente(cliente);
    }
    public Servico find(Tecnico tecnico) {
        return repository.findByTecnico(tecnico);
    }
}