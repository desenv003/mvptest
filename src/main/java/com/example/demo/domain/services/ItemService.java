package com.example.demo.domain.services;

import java.util.List;
import com.example.demo.domain.model.Item;
import com.example.demo.domain.model.Servico;
import com.example.demo.domain.repositories.ItemRepository;
import com.example.demo.domain.utils.StringTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ItemService {
    @Autowired
    private ItemRepository repository;

    public Item save(String desc, Servico servico) {
        Item item = Item.builder()
                .desc(desc)
                .servico(servico)
                .build();

        return repository.saveAndFlush(item);
    }

    @Transactional
    public void update(Item item, String newDesc, Servico newServico) {
        if(StringTypeUtils.validate(newDesc)){
            item.setDesc(newDesc);
            item.setServico(newServico);
            repository.save(item);
        }
    }

    public void delete(Item item) {
        repository.delete(item);
    }

    public List<Item> find() {
        return repository.findAll();
    }

    public Item find(Servico servico) {
        return repository.findByServico(servico);
    }
}