package com.example.demo.domain.services;

import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.config.exceptions.errors.ErrorMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class ClienteValidateService {
    @Autowired
    private ClienteService clienteService;

    protected void validate(String name) throws RecordFoundException {
        if (clienteService.find(name) != null) {
            throw new RecordFoundException(ErrorMessages.RECORD_FOUND.getMessage());
        }
    }
}