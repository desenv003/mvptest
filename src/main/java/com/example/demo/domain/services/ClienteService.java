package com.example.demo.domain.services;

import java.util.List;
import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.model.Cliente;
import com.example.demo.domain.repositories.ClienteRepository;
import com.example.demo.domain.utils.StringTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository repository;

    @Autowired
    private ClienteValidateService validateService;

    public Cliente save(String name) throws RecordFoundException {
        validateService.validate(name);

        Cliente cliente = Cliente.builder()
                .name(name)
                .build();

        return repository.saveAndFlush(cliente);
    }

    @Transactional
    public void update(Cliente cliente, String newName) {
        if(StringTypeUtils.validate(newName)) {
            cliente.setName(newName);
            repository.save(cliente);
        }
    }

    public List<Cliente> find() {
        return repository.findAll();
    }

    public Cliente find(String nome) {
        return repository.findByName(nome);
    }

    public void delete(Cliente cliente) {
        repository.delete(cliente);
    }
}