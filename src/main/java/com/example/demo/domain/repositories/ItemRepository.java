package com.example.demo.domain.repositories;

import com.example.demo.domain.model.Item;
import com.example.demo.domain.model.Servico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    Item findByServico(Servico servico);
}