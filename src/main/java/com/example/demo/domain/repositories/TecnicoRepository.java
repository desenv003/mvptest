package com.example.demo.domain.repositories;

import com.example.demo.domain.model.Tecnico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TecnicoRepository extends JpaRepository<Tecnico, Long> {
    Tecnico findByName(String name);
}