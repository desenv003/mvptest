package com.example.demo.domain.model;

import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.AccessLevel;
import javax.persistence.Entity;
import javax.persistence.UniqueConstraint;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Item", uniqueConstraints = @UniqueConstraint(name = "UCItem", columnNames = {"Descricao", "CODServico"}))
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class Item {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ITEM_ID")
    @SequenceGenerator(
            name = "SEQ_ITEM_ID",
            allocationSize = 1,
            sequenceName = "SEQ_ITEM_ID"
    )
    private Long id;

    @Getter
    @Setter
    @NotNull(message = "Descricao não pode ser nulo")
    @Column(name = "Descricao")
    private String desc;

    @Getter
    @Setter
    @NotNull(message = "Código do serviço não pode ser nulo")
    @ManyToOne
    @JoinColumn(name = "CODServico")
    private Servico servico;
}