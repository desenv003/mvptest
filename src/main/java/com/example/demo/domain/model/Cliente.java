package com.example.demo.domain.model;

import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.Builder;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Cliente", uniqueConstraints = @UniqueConstraint(name = "UCCliente", columnNames = {"Nome"}))
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class Cliente {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_CLIENTE_ID")
    @SequenceGenerator(
            name = "SEQ_CLIENTE_ID",
            allocationSize = 1,
            sequenceName = "SEQ_CLIENTE_ID"
    )
    private Long id;

    @Getter
    @Setter
    @NotNull(message = "Nome do cliente não pode ser nulo")
    @Column(name = "Nome")
    private String name;
}