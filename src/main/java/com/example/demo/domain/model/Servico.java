package com.example.demo.domain.model;

import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.AccessLevel;
import javax.persistence.Entity;
import javax.persistence.UniqueConstraint;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "Servico", uniqueConstraints = @UniqueConstraint(name = "UCServico", columnNames = {"CODCliente", "CODTecnico", "Status", "Procedencia"}))
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class Servico {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SERVICO_ID")
    @SequenceGenerator(
            name = "SEQ_SERVICO_ID",
            allocationSize = 1,
            sequenceName = "SEQ_SERVICO_ID"
    )
    private Long id;

    @Getter
    @Setter
    @NotNull(message = "Número do serviço não pode ser nulo")
    @Column(name = "NumeroServico")
    private int numeroServico;

    @Getter
    @Setter
    @NotNull(message = "Status não pode ser nulo")
    @Column(name = "Status")
    private String status;

    @Getter
    @Setter
    @NotNull(message = "Procedencia não pode ser nulo")
    @Column(name = "Procedencia")
    private boolean procedencia;

    @Getter
    @Setter
    @NotNull(message = "Código do cliente não pode ser nulo")
    @ManyToOne
    @JoinColumn(name = "CODCliente")
    private Cliente cliente;

    @Getter
    @Setter
    @NotNull(message = "Código do técnico não pode ser nulo")
    @ManyToOne
    @JoinColumn(name = "CODTecnico")
    private Tecnico tecnico;

    @Getter
    @Setter
    @NotNull(message = "Data da solicitação não pode ser nula")
    @Column(name = "DataSolicitacao")
    private LocalDate dataSolicitacao;

    @Getter
    @Setter
    @NotNull(message = "Data do técnico não pode ser nula")
    @Column(name = "DataTecnico")
    private LocalDate dataTecnico;

    public boolean getProcedencia(){
        return this.procedencia;
    }
}