package com.example.demo.domain.model;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Tecnico", uniqueConstraints = @UniqueConstraint(name = "UCTecnico", columnNames = {"Nome", "Disponibilidade"}))
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class Tecnico {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_TECNICO_ID")
    @SequenceGenerator(
            name = "SEQ_TECNICO_ID",
            allocationSize = 1,
            sequenceName = "SEQ_TECNICO_ID"
    )
    private Long id;

    @Getter
    @Setter
    @NotNull(message = "Técnico não pode ser nulo")
    @Column(name = "Nome")
    private String name;

    @Getter
    @Setter
    @NotNull(message = "Disponibilidade não pode ser nula")
    @Column(name = "Disponibilidade")
    private boolean disponibilidade;

    public boolean getDisponibilidade(){
        return this.disponibilidade;
    }
}