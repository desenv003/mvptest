package com.example.demo.domain.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class DateTypeUtils {
    private DateTypeUtils(){}

    public static boolean validate(String dateVerify){
        if(dateVerify == null || dateVerify.equals("")) {
            return false;
        } else {
            SimpleDateFormat sdfrmt = new SimpleDateFormat("dd/MM/yyyy");

            try {
                sdfrmt.parse(dateVerify);
            } catch (ParseException e) {
                return false;
            }
            return true;
        }
    }
}