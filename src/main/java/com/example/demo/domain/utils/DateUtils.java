package com.example.demo.domain.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public final class DateUtils {
    private DateUtils(){}

    private static final String PATTERN = "dd/MM/yyyy";
    private static final String PATTERN_TIME = "dd/MM/yyyy HH:mm";

    public static LocalDate getParse(String data) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN);
        return LocalDate.parse(data, formatter);
    }

    public static Date getDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDate(String data) {
        LocalDate localDate = getParse(data);
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate getLocalDate(Date data) {
        return data.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime getParseTime(String data) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_TIME);
        return LocalDateTime.parse(data,formatter);
    }

    public static LocalDateTime getLocalDateTime(Date data) {
        return LocalDateTime.ofInstant(data.toInstant(), ZoneId.systemDefault());
    }
}