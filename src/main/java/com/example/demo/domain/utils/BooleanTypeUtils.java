package com.example.demo.domain.utils;

import static java.util.Objects.isNull;

public class BooleanTypeUtils {
    public static boolean validate(boolean booleanVerify){
        return !isNull(booleanVerify) && booleanVerify;
    }
}