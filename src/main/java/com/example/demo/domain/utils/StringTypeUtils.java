package com.example.demo.domain.utils;

import java.util.regex.Pattern;

public final class StringTypeUtils {
    public static boolean validate(String stringVerify) {
        return !(stringVerify == null || stringVerify.equals(""));
    }

    public static boolean validateSpecial(String stringVerify) {
        String thePattern = "[^A-Za-z]+";

        return !Pattern.compile(thePattern).matcher(stringVerify).find();
    }
}