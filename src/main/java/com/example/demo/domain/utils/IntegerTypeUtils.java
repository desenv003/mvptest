package com.example.demo.domain.utils;

public final class IntegerTypeUtils {
    private IntegerTypeUtils(){}

    public static boolean validate(int integerVerify) {
        return integerVerify >= 0;
    }
}