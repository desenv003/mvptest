package com.example.demo.config.exceptions;

public class RecordFoundException extends Exception {
    public RecordFoundException(String message) {
        super(message);
    }
}