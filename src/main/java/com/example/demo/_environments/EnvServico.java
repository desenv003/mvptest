package com.example.demo._environments;

import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.model.Cliente;
import com.example.demo.domain.model.Tecnico;
import com.example.demo.domain.repositories.ClienteRepository;
import com.example.demo.domain.repositories.TecnicoRepository;
import com.example.demo.domain.services.ServicoService;
import com.example.demo.domain.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
public class EnvServico {
    @Autowired
    private ServicoService service;

    @Autowired
    private EnvCliente envCliente;
    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EnvTecnico envTecnico;
    @Autowired
    private TecnicoRepository tecnicoRepository;

    public void init() throws RecordFoundException {
        envCliente.init();
        envTecnico.init();

        Date dateServico = DateUtils.getDate("01/12/2016");
        Date dateTecnico = DateUtils.getDate("01/12/2016");

        Cliente primeiro = clienteRepository.findByName("user1");
        Tecnico primeiroT = tecnicoRepository.findByName("user1");

        service.save(1,"Em produção", true, primeiro, primeiroT, DateUtils.getLocalDate(dateServico), DateUtils.getLocalDate(dateTecnico));
    }
}