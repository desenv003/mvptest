package com.example.demo._environments;

import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.services.TecnicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnvTecnico {
    private final TecnicoService tecnicoService;

    @Autowired
    public EnvTecnico(TecnicoService tecnicoService) {
        this.tecnicoService = tecnicoService;
    }

    public void init() throws RecordFoundException {
        tecnicoService.save("user1", true);
    }
}