package com.example.demo._environments;

import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnvCliente {
    private final ClienteService clienteService;

    @Autowired
    public EnvCliente(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    public void init() throws RecordFoundException {
        clienteService.save("user1");
        clienteService.save("user2");
    }
}