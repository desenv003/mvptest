package com.example.demo._environments;

import com.example.demo.config.exceptions.RecordFoundException;
import com.example.demo.domain.repositories.ServicoRepository;
import com.example.demo.domain.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnvItem {
    @Autowired
    private ItemService service;

    @Autowired
    private EnvServico envServico;
    @Autowired
    private ServicoRepository servicoRepository;

    public void init() throws RecordFoundException {
        envServico.init();

        service.save("Piso rachado", servicoRepository.findByNumeroServico(1));
    }
}